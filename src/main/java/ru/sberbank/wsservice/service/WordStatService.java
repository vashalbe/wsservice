package ru.sberbank.wsservice.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.sberbank.wsservice.Util.WordStatUtil;
import ru.sberbank.wsservice.entity.GetWordstatReportListResponseV4;
import ru.sberbank.wsservice.entity.V4ReportIdAndStatus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

@Service
public class WordStatService {
    @Value("${yandex.token}")
    private String token;
    @Value("${yandex.host}")
    private String host;

    public Mono<Integer> getShowsByWord(String word) throws Exception {
        int reportId = createReportByOneWord(word);
        waitForReportIsDone(reportId);
        int shows = getSwowsByReportId(reportId);
        deleteReportById(reportId);

        return Mono.just(shows);
    }

    private void waitForReportIsDone(int reportId) throws Exception {
        boolean reportIsDone = false;

        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            reportIsDone = isReportDoneById(reportId);
            if (reportIsDone) {
                break;
            }
        }

        if (!reportIsDone) {
            throw new Exception("Error. Report was not done during 10 seconds");
        }
    }

    private int createReportByOneWord(String word) {

        try {
            String jsonInputString = "{\"token\": \"" + token + "\", \"method\": \"CreateNewWordstatReport\", \"param\": {\"Phrases\": [\"" + word + "\"], \"GeoID\": [0]}}";
            HttpURLConnection con = getConfiguredHttpURLConnection(jsonInputString);

            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                return WordStatUtil.mapResponseWithReportIdToReportId(response.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    private boolean isReportDoneById(int id) {
        GetWordstatReportListResponseV4 existingReportList = getExistingReportList();
        V4ReportIdAndStatus searchedReport = null;

        for (V4ReportIdAndStatus reportIdAndStatus : existingReportList.getData()) {
            if (reportIdAndStatus.getReportID() == id) {
                searchedReport = reportIdAndStatus;
            }
        }
        return searchedReport != null && searchedReport.getStatusReport().equals("Done");
    }

    private GetWordstatReportListResponseV4 getExistingReportList() {
        try {
            HttpURLConnection con = getConfiguredHttpURLConnection("{\"token\": \"" + token + "\", \"method\": \"GetWordstatReportList\"}");

            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }

                return WordStatUtil.mapGetWordstatReportListResponseFromJson(response.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private int getSwowsByReportId(int id) {
        try {
            String jsonInputString = "{\"token\": \"" + token + "\", \"method\": \"GetWordstatReport\", \"param\": " + id + "}";
            HttpURLConnection con = getConfiguredHttpURLConnection(jsonInputString);

            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;

                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }

                return WordStatUtil.mapGetWordstatReportResponseFromJson(response.toString()).getData().get(0).getSearchedWith().get(0).getShows();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    private void deleteReportById(int reportId) {
        Thread thread = new Thread(() -> {
            try {
                String jsonInputString = "{\"token\": \"" + token + "\", \"method\": \"DeleteWordstatReport\", \"param\": " + reportId + "}";
                HttpURLConnection con = getConfiguredHttpURLConnection(jsonInputString);

                try (BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;

                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        thread.start();
    }

    private HttpURLConnection getConfiguredHttpURLConnection(String json) throws IOException {
        URL url = new URL(host);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.addRequestProperty("Content-Type", "application/json");
        con.addRequestProperty("Accept-Language", "en");
        con.setDoOutput(true);

        try (OutputStream os = con.getOutputStream()) {
            byte[] input = json.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        return con;
    }
}
