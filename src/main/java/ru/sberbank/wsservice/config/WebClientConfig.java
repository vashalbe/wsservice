package ru.sberbank.wsservice.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.web.reactive.function.client.WebClient;

import javax.annotation.Resource;

@Configuration
public class WebClientConfig {
    @Value("${yandex.host}")
    private String baseUrl;

    @Bean
    public WebClient wordStatWebClient() {
        return WebClient.builder()
                        .baseUrl(baseUrl)
                        .build();
    }

}
