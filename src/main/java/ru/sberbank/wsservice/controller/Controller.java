package ru.sberbank.wsservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import ru.sberbank.wsservice.service.WordStatService;

@RestController
@RequiredArgsConstructor
public class Controller {
    private final WordStatService wordStatService;

    @GetMapping("/{word}")
    public Mono<Integer> gogo(@PathVariable String word) throws Exception {
        return wordStatService.getShowsByWord(word);
    }
}
