package ru.sberbank.wsservice.Util;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import ru.sberbank.wsservice.entity.GetWordstatReportListResponseV4;
import ru.sberbank.wsservice.entity.GetWordstatReportResponseV4;


public class WordStatUtil {

    public static int mapResponseWithReportIdToReportId(String responseWithReportId){
        JsonObject jsonObject = new JsonParser().parse(responseWithReportId.toString()).getAsJsonObject();
        JsonElement jsonElement = jsonObject.get("data");
        return jsonElement.getAsInt();
    }

    public static GetWordstatReportListResponseV4 mapGetWordstatReportListResponseFromJson(String json){
        Gson gson = new Gson();
        return gson.fromJson(json, GetWordstatReportListResponseV4.class);
    }

    public static GetWordstatReportResponseV4 mapGetWordstatReportResponseFromJson(String json){
        Gson gson = new Gson();
        return gson.fromJson(json, GetWordstatReportResponseV4.class);
    }
}
