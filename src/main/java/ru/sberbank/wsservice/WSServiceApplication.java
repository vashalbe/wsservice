package ru.sberbank.wsservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WSServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(WSServiceApplication.class, args);
	}
}
