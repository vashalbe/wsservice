package ru.sberbank.wsservice.entity;

import lombok.Getter;

import java.util.List;

@Getter
public class GetWordstatReportListResponseV4 {

    private List<V4ReportIdAndStatus> data;
}
