package ru.sberbank.wsservice.entity;

import lombok.Getter;

import java.util.List;

@Getter
public class GetWordstatReportResponseV4 {

    private List<WordstatReportInfoV4> data;
}
