package ru.sberbank.wsservice.entity;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.List;

@Getter
public class WordstatReportInfoV4 {

    @SerializedName("Phrase")
    private String phrase;
    @SerializedName("GeoID")
    private List<Integer> geoId;
    @SerializedName("SearchedWith")
    private List<WordstatItemV4> searchedWith;
}
