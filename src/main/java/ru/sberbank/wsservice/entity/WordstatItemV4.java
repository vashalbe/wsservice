package ru.sberbank.wsservice.entity;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class WordstatItemV4 {

    @SerializedName("Phrase")
    private String phrase;
    @SerializedName("Shows")
    private int shows;
}
