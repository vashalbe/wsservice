package ru.sberbank.wsservice.entity;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class V4ReportIdAndStatus {

    @SerializedName("ReportID")
    private int reportID;
    @SerializedName("StatusReport")
    private String statusReport;
}
